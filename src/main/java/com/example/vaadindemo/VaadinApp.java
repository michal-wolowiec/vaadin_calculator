package com.example.vaadindemo;

import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@Title("Vaadin Demo App")
public class VaadinApp extends UI {

	private static final long serialVersionUID = 1L;

	boolean add = false;
	boolean sub = false;
	boolean mul = false;
	boolean div = false;
	int number = 0;
	
	@Override
	protected void init(VaadinRequest request) {

		
		
		
		GridLayout gl = new GridLayout(6, 6);	
		
		final Button oneBtn = new Button("1");
		final Button twoBtn = new Button("2");
		final Button threeBtn = new Button("3");
		final Button fourBtn = new Button("4");
		final Button fiveBtn = new Button("5");
		final Button sixBtn = new Button("6");
		final Button sevenBtn = new Button("7");
		final Button eightBtn = new Button("8");
		final Button nineBtn = new Button("9");
		final Button zeroBtn = new Button("0");
		final Button cBtn = new Button("C");
		final Button equalsBtn = new Button("=");
		
		final Button addBtn = new Button("ADD");
		final Button subBtn = new Button("SUB");
		final Button mulBtn = new Button("MUL");
		final Button divBtn = new Button("DIV");

		final TextField fieldOne = new TextField();
			
		
		gl.addComponent(fieldOne, 0, 0);
		gl.addComponent(oneBtn, 1, 2);
		gl.addComponent(twoBtn, 2, 2);
		gl.addComponent(threeBtn, 3, 2);
		gl.addComponent(fourBtn, 1, 3);
		gl.addComponent(fiveBtn, 2, 3);
		gl.addComponent(sixBtn, 3, 3);
		gl.addComponent(sevenBtn, 1, 4);
		gl.addComponent(eightBtn, 2, 4);
		gl.addComponent(nineBtn, 3, 4);
		gl.addComponent(zeroBtn, 2, 5);
		gl.addComponent(cBtn, 3, 5);
		gl.addComponent(equalsBtn, 1, 5);
		
		gl.addComponent(addBtn, 0, 1);
		gl.addComponent(subBtn, 0, 2);
		gl.addComponent(mulBtn, 0, 3);
		gl.addComponent(divBtn, 0, 4);
		
		
		
		
		// buttons
		addBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String numberOne = fieldOne.getValue();
				
				add = true;
				number = Integer.parseInt(numberOne);
				fieldOne.setValue("");
				//2. helloLbl.setValue(helloTF.getValue());				

			}
		});
		
		subBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String numberOne = fieldOne.getValue();
				
				sub = true;
				number = Integer.parseInt(numberOne);
				fieldOne.setValue("");
				//2. helloLbl.setValue(helloTF.getValue());				

			}
		});
		
		mulBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String numberOne = fieldOne.getValue();
				
				mul = true;
				number = Integer.parseInt(numberOne);
				fieldOne.setValue("");
				//2. helloLbl.setValue(helloTF.getValue());				

			}
		});
		
		divBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String numberOne = fieldOne.getValue();
				
				div = true;
				number = Integer.parseInt(numberOne);
				fieldOne.setValue("");
				//2. helloLbl.setValue(helloTF.getValue());				

			}
		});
		
		
		oneBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String number = fieldOne.getValue();

				fieldOne.setValue(number + "1");				

			}
		});
		
		twoBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String number = fieldOne.getValue();

				fieldOne.setValue(number + "2");				

			}
		});
		
		threeBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String number = fieldOne.getValue();

				fieldOne.setValue(number + "3");				

			}
		});
		
		fourBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String number = fieldOne.getValue();

				fieldOne.setValue(number + "4");				

			}
		});
		
		fiveBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String number = fieldOne.getValue();

				fieldOne.setValue(number + "5");				

			}
		});
		
		sixBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String number = fieldOne.getValue();

				fieldOne.setValue(number + "6");				

			}
		});
		
		sevenBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String number = fieldOne.getValue();

				fieldOne.setValue(number + "7");				

			}
		});
		
		eightBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String number = fieldOne.getValue();

				fieldOne.setValue(number + "8");				

			}
		});
		
		nineBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String number = fieldOne.getValue();

				fieldOne.setValue(number + "9");				

			}
		});
		
		zeroBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String number = fieldOne.getValue();

				fieldOne.setValue(number + "0");				

			}
		});
		
		cBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				fieldOne.setValue("");				

			}
		});
		
		equalsBtn.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				String numberOne = fieldOne.getValue();				
				int localNumber = Integer.parseInt(numberOne);
				
				if(add){
					fieldOne.setValue("" + (number+localNumber));
					add = false;
				}
				if(sub){
					fieldOne.setValue("" + (number-localNumber));
					sub = false;
				}
				if(mul) {
					fieldOne.setValue("" + (number*localNumber));
					mul = false;
				}
				if(div) {
					fieldOne.setValue("" + (number/localNumber));
					div = false;
				}
							

			}
		});
		
		
			
		setContent(gl);				
	}
}
